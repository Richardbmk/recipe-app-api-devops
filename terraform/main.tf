provider "aws" {
  region  = "us-east-2"
  version = "~> 2.50.0"
}



terraform {
  backend "s3" {
    bucket         = "rick-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    encrypt        = true
    region         = "us-east-2"
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}